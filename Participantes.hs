module Participantes where

type Participantes = [Corredor]

data Corredor
  = Corr { equipo:: Equipo, nom:: Nombre, dorsal:: Dorsal }
    deriving  Show

type Equipo = String
type Nombre = String
type Dorsal = Int

participantes:: Participantes
participantes = [corr1, corr2, corr3, corr4, corr5, corr6,
                 corr7, corr8, corr9, corr10,corr11, corr12]

corr1, corr2, corr3, corr4, corr5, corr6:: Corredor
corr7, corr8, corr9, corr10, corr11, corr12:: Corredor

corr1 = Corr "TEAM INEOS" "G. Thomas" 1
corr2 = Corr "TEAM INEOS" "E. Bernal" 2
corr3 = Corr "TEAM INEOS" "J. Castroviejo" 3

corr4 = Corr "MOVISTAR TEAM" "A. Amador" 4
corr5 = Corr "MOVISTAR TEAM" "I. Erviti" 5
corr6 = Corr "MOVISTAR TEAM" "M. Landa" 6

corr7 = Corr "TEAM JUMBO-VISMA" "A. Jansen" 7
corr8 = Corr "TEAM JUMBO-VISMA" "T. Martin" 8
corr9 = Corr "TEAM JUMBO-VISMA" "M. Teunissen" 9

corr10 = Corr "TEAM ARKEA-SAMSIC" "K. Ledanois" 10
corr11 = Corr "TEAM ARKEA-SAMSIC" "A. Moinard" 11
corr12 = Corr "TEAM ARKEA-SAMSIC" "F. Vachon" 12
