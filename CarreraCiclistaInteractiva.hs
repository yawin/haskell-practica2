-----------------------------------------------------------------
--  PRACTICA: CARRERA CICLISTA	INTERACTIVA     --    PF  2019-20

--  Nombre(s): Miguel Albors
-----------------------------------------------------------------

module CarreraCiclistaInteractiva where

import CarreraCiclista

type Archivo = String

-----------------------------------------------------
--     ESCRITURA de una Carrera en un FICHERO      --
-----------------------------------------------------

-- guardaCarrera: guarda la carrera en un archivo.
-- El fichero esta en el directorio desde el que hemos arrancado
guardaCarrera :: Archivo -> Carrera ->  IO ()
guardaCarrera archivo carr = writeFile archivo (pasarAstring carr)

-- pasarAstring: string con la informaci�n de una carrera
pasarAstring :: Carrera -> String
pasarAstring [] = ""
pasarAstring (x:xs) = cadena ++ "\n" ++ pasarAstring xs
    where cadena = show x



-----------------------------------------------
--     LECTURA DE FICHERO con una Carrera    --
-----------------------------------------------

-- recuperaCarrera: recupera la carrera del archivo dado.
-- El fichero debe estar en el directorio desde el que arrancamos
-- y su contenido ha sido formado por guardaCarrera.
-- Algoritmo:
-- 1) Se lee todo el contenido del archivo (que es un string)
-- 2) A partir de dicho string se recupera la Carrera
recuperaCarrera :: Archivo -> IO Carrera
recuperaCarrera archivo = do
                           texto <- readFile archivo
                           return (hazCarrera texto)

-- hazCarrera: crea una carrera con la informaci�n del string dado
hazCarrera :: String -> Carrera
hazCarrera [] = []
hazCarrera s = hazEtapa (takeWhile (/='\n') s) : hazCarrera (dropWhile (/='E') (dropWhile (/='\n') s))

hazEtapa :: String -> Etapa
hazEtapa s = Etap {num = atoi (fst numero), tipo = atoetap (fst tipoEtapa), tiempos = atotiemposetapa (fst tiempo)}
    where numero = getDato "Etap {num = " ',' s
          tipoEtapa = getDato ", tipo = " ',' (snd numero)
          tiempo = getDato ", tiempos = " '}' (snd tipoEtapa)

getDato :: String -> Char -> String -> (String, String)
getDato "" l s = (takeWhile (/=l) s, dropWhile (/=l) s)
getDato (x:xs) l s = getDato xs l (dropWhile (/=x) s)


atoi :: String -> Int
atoi s = read s

atoetap :: String -> TipoEtapa
atoetap s = read s::TipoEtapa

atotiemposetapa :: String -> TiemposEtapa
atotiemposetapa s = read s::TiemposEtapa

-----------------------------------------------
--     LECTURA DE FICHERO con una Etapa    --
-----------------------------------------------

-- recuperaEtapa: recupera la etapa del archivo dado.
-- El fichero debe estar en el directorio desde el que arrancamos.
recuperaEtapa :: Archivo -> IO Etapa
recuperaEtapa archivo = do
                           texto <- readFile archivo
                           return (recEtapa texto)

recEtapa :: String -> Etapa
recEtapa s = Etap {num = atoi (fst numero), tipo = atoetap (fst tipoEtapa), tiempos = getTiempos (snd tipoEtapa)}
    where numero = (takeWhile (/='\n') s, tail (dropWhile (/='\n') s))
          tipoEtapa = (takeWhile (/='\n') (snd numero), tail (dropWhile (/='\n') (snd numero)))

getTiempos :: String -> TiemposEtapa
getTiempos s
    | length s < 7 = []
    | otherwise = tiempos : getTiempos (snd segundos)
                    where tiempos = (atoi(fst dorsal), (atoi(fst horas), atoi(fst minutos), atoi(fst segundos)))
                          dorsal = getDato "" ' ' s
                          horas = getDato "" ' ' (tail (snd dorsal))
                          minutos = getDato "" ' ' (tail (snd horas))
                          segundos = getDato "" '\n' (tail (snd minutos))
---------------------------------------------------
--     Interacci�n con la carrera    (main)      --
---------------------------------------------------

main :: IO()
main = gestCarrera participantes "carrera.txt"
-- Programa principal. PRECONDICIONES:
-- Espera que el fichero con los datos de la carrera actual se
-- encuentre en el archivo "carrera.txt" (y los de cada etapa
-- a incluir esten en los ficheros etapa1.txt, etapa2.txt, ..)
-- Usa la constante participantes del modulo Participantes.


-- Muestra el menu de opciones, recupera la informaci�n del archivo
-- e inicia una sesion.
gestCarrera :: Participantes -> Archivo -> IO ()
gestCarrera p a = do
                    c <- recuperaCarrera a
                    if length c == 0 then putStr "\n\nADVERTENCIA: La carrera no contiene datos\n\n"
                    else putStr ""
                    sesionCon c p

---------------------------------------------------
--              MEN� de ORDENES                  --
---------------------------------------------------

-- Texto del menu de opciones.
menu :: String
menu = (guiones 20) ++"\n  CARRERA CICLISTA  \n" ++ (guiones 20) ++ "\nOrdenes disponibles: \n" ++
       "  1. Mostrar participantes\n" ++
       "  2. Mostrar la carrera actual\n" ++
       "  3. Introducir etapa en la carrera actual\n" ++
       "  4. Mostrar la clasificacion por corredor \n" ++
       "  5. Mostrar la clasificacion por equipos \n" ++
       "  6. Mostrar el lider de un tipo de etapa \n" ++
       "  7. Mostrar el lider de la carrera \n" ++
       "  8. Mostrar los abandonos \n" ++
       "  9. Mostrar carrera, guardarla y finalizar sesion \n"

guiones :: Int -> String
guiones 1 = "-"
guiones n = "-" ++ guiones (n-1)

---------------------------------------------------
--              CADA SESION INTERACTIVA           --
---------------------------------------------------

opc :: Carrera -> Participantes -> Char -> IO()
opc c p '1' = do
                mostrarParticipantes p
                sesionCon c p
opc c p '2' = do
                if length c > 0 then mostrarCarrera c
                else putStr "No hay etapas cargadas"
                sesionCon c p
opc c p '3' = do
                putStr "\nIntroduce el nombre del fichero a cargar: "
                fich <- getLine
                e <- recuperaEtapa fich
                clearScreen
                mostrarCarrera [e]
                sesionCon (introducirEtapa e c) p
opc c p '4' = do
                mostrarClasifCorr c p
                sesionCon c p
opc c p '5' = do
                mostrarClasifEqui c p
                sesionCon c p
opc c p '6' = do
                putStr "Selecciona el tipo de etapa: [M]ontagna, [T]iempo (contra reloj) o [C]arretera "
                t <- getChar
                clearScreen
                porTipo c p t
                sesionCon c p
opc c p '7' = do
                mostrarLider c p
                sesionCon c p
opc c p '8' = do
                mostrarAbandonos c p
                sesionCon c p
opc c p '9' = do
                guardaCarrera "carrera.txt" c
                if length c > 0 then mostrarCarrera c
                else putStr "No hay etapas cargadas"

                putStrLn "Datos almacenados correctamente\nPulsa cualquier tecla para salir\n"
                o <- getChar
                clearScreen
opc c p _ = do
                    putStr "Opcion incorrecta\n"
                    sesionCon c p
--sesionCon:: Carrera -> Participantes -> IO()
-- Proceso principal:
-- Su efecto es
--    pedir una opci�n al usuario;
--    ejecutar la orden correspondiente
sesionCon :: Carrera -> Participantes -> IO ()
sesionCon c p = do
                  putStr "\n\n\n"
                  putStr menu
                  o <- getChar
                  clearScreen
                  opc c p o

clearScreen :: IO()
clearScreen = putStr "\ESC[2J\n"

porTipo :: Carrera -> Participantes -> Char -> IO()
porTipo c p 'M' = mostrarLiderTipo c p Montagna
porTipo c p 'm' = mostrarLiderTipo c p Montagna
porTipo c p 'T' = mostrarLiderTipo c p Contra_reloj
porTipo c p 't' = mostrarLiderTipo c p Contra_reloj
porTipo c p 'C' = mostrarLiderTipo c p Carretera
porTipo c p 'c' = mostrarLiderTipo c p Carretera
porTipo c p _ = putStr "Opcion incorrecta"
