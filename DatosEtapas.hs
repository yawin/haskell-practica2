module DatosEtapas where

import CarreraCiclista

miCarrera :: Carrera
miCarrera = [etapa1, etapa2, etapa3]

etapa1, etapa2, etapa3, etapa4 :: Etapa
etapa1 = Etap 1 Montagna tiemposEtapa1
etapa2 = Etap 2 Carretera tiemposEtapa2
etapa3 = Etap 3 Montagna tiemposEtapa3
etapa4 = Etap 4 Carretera tiemposEtapa4

tiemposEtapa1, tiemposEtapa2, tiemposEtapa3, tiemposEtapa4 :: TiemposEtapa

tiemposEtapa1 = [(1,(4,22,47)),(2,(4,22,52)),(5,(4,22,52)),
                 (3,(4,23,0)),(6,(4,23,0)),(4,(4,23,10)),
                 (7,(4,23,20)),(8,(4,23,33)),(9,(4,23,40)),
                 (10,(4,23,50)),(11,(4,24,0)),(12,(4,24,10))]

tiemposEtapa2 = [(4,(0,0,0)),(6,(3,23,20)),(3,(3,50,10)),
                 (5,(4,22,52)),(7,(4,23,20)),(8,(4,23,33)),
                 (9,(4,23,40)),(10,(4,23,50)),(11,(4,24,0)),
                 (12,(4,24,10)),(2,(4,25,52)),(1,(4,30,27))]

tiemposEtapa3 = [(9,(0,0,0)),(6,(3,23,20)),(3,(3,50,10)),
                 (5,(4,22,52)),(7,(4,23,20)),(8,(4,23,33)),
                 (10,(4,23,50)),(11,(4,24,0)),(12,(4,24,10)),
                 (2,(4,25,52)),(1,(4,30,27))]

tiemposEtapa4 = [(10,(0,0,0)),(8,(2,54,48)),(11,(2,55,59)),
                 (1,(4,22,47)),(2,(4,22,52)),(3,(4,23,0)),
                 (6,(4,23,0)),(7,(4,23,20)),(5,(4,23,50)),
                 (12,(4,24,10))]
