------------------------------------------------------------------
--  PRACTICA: cARRERA CICLISTA	      --               PF  2019-20

--  Nombre(s): Miguel Albors
------------------------------------------------------------------

module CarreraCiclista (module Participantes,
                        module CarreraCiclista) where

import Participantes

-----------------------------------------------
--      TIPOS DESCRITOS EN EL ENUNCIADO      --
-----------------------------------------------

type Carrera = [Etapa]

data Etapa = Etap {num:: Int, tipo:: TipoEtapa,
                    tiempos:: TiemposEtapa } deriving Show

type TiemposEtapa = [(Dorsal, Tiempo)]
data TipoEtapa = Montagna | Contra_reloj | Carretera
                 deriving (Eq, Show, Read)
type Tiempo = (Int, Int, Int)

type Clasificacion = (Equipo, Nombre, Tiempo)

-----------------------------------------------
--          OPERACIONES PRINCIPALES          --
-----------------------------------------------

-- introducirEtapa :: Etapa -> Carrera -> Carrera
-- introducirEtapa et c: introduce en la carrera la etapa dada
introducirEtapa :: Etapa -> Carrera -> Carrera
introducirEtapa et c = c ++ [et]

-- obtClasificacion:: Carrera -> Participantes -> [(Equipo, Nombre, Tiempo)]
-- obtClasificacion c p: obtiene los tiempos de los corredores que siguen en la carrera
obtClasificacion :: Carrera -> Participantes -> [Clasificacion]
obtClasificacion c p = ordenaClasificacion (obtClasificacionSinOrd c p)

obtClasificacionSinOrd :: Carrera -> Participantes -> [Clasificacion]
obtClasificacionSinOrd c [x] = _obtClasificacion (equipo x) (nom x) (obtTiempos c x)
obtClasificacionSinOrd c (x:p) = (_obtClasificacion (equipo x) (nom x) (obtTiempos c x)) ++ (obtClasificacionSinOrd c p)

-- Comprueba si el corredor sigue en carrera y en caso afirmativo, suma sus tiempos.
-- Si sigue en carrera devuelve una Clasificación si no, devuelve lista vacía
_obtClasificacion :: Equipo -> Nombre -> [Tiempo] -> [Clasificacion]
_obtClasificacion e n ts = if not (elem (0,0,0) ts) then [(e, n, foldl sumaTiempos (0,0,0) ts)] else []

-- obtClasifEquipos:: Carrera -> Participantes -> [(Equipo, Tiempo)]
-- obtClasifEquipos c p: obtiene los tiempos de los equipos que participan en la carrera
obtClasifEquipos :: Carrera -> Participantes -> [(Equipo, Tiempo)]
obtClasifEquipos c p = ordenaClasificacionEquipos (obtClasifEquiposSinOrd c p)

obtClasifEquiposSinOrd :: Carrera -> Participantes -> [(Equipo, Tiempo)]
obtClasifEquiposSinOrd c p = _obtClasifEquipos (obtEquipos clasif) clasif where
        clasif = obtClasificacionSinOrd c p

_obtClasifEquipos :: [Equipo] -> [Clasificacion] -> [(Equipo, Tiempo)]
_obtClasifEquipos [] cl = []
_obtClasifEquipos (e:es) cl = (e, foldl sumaTiempos (0,0,0) [t | (eq, nom, t) <- cl, eq == e]) : _obtClasifEquipos es cl

-- obtLider:: Carrera -> Participantes -> (Nombre, Equipo)
-- obtLider c p: obtiene el lider de la carrera
obtLider :: Carrera -> Participantes -> (Nombre, Equipo)
obtLider c p = if t /= (0,0,0) then (n, e) else ("", "")
    where ((n, e, t):ls) = obtClasificacion c p

-- obtLiderTipo:: Carrera -> Participantes -> TipoEtapa -> (Nombre, Equipo)
-- obtLiderTipo c p tipo: obtiene el lider de la carrera en el tipo de etapa dado
obtLiderTipo :: Carrera -> Participantes -> TipoEtapa -> (Nombre, Equipo)
obtLiderTipo c p t = obtLider [x | x <-c, (tipo x) == t] p

-- retiradosCarrera:: Participantes -> Carrera -> [(Nombre, Int)]
-- retiradosCarrera p c: obtiene los corredores que se han retirado de la carrera
-- y la etapa en la que han abandonado
retiradosCarrera :: Participantes -> Carrera -> [(Nombre, Int)]
retiradosCarrera [] c = []
retiradosCarrera (p:ps) c = (retiradoCarrera c p) ++ (retiradosCarrera ps c)

retiradoCarrera :: Carrera -> Corredor -> [(Nombre, Int)]
retiradoCarrera [] c = []
retiradoCarrera ((Etap num tipo tiempos):xs) c = if tiempo == (0,0,0) then [(nom c, num)] else retiradoCarrera xs c
        where tiempo = head [x | (d, x) <- tiempos, d == (dorsal c)]
---------------------------------------------------
--    VISUALIZACIÓN DE DATOS Y OTRAS OPERACIONES            --
---------------------------------------------------

obtEquipos :: [Clasificacion] -> [Equipo]
obtEquipos [] = []
obtEquipos ((e,_,_):cs) = if not (elem e resul) then e:resul else resul where
        resul = obtEquipos cs

-- Obtiene los tiempos de un corredor en cada etapa
obtTiempos :: Carrera -> Corredor -> [Tiempo]
obtTiempos [] co = []
obtTiempos ((Etap num tipo tiempos):cs) co = (obtTiempos cs co) ++ [x | (d, x) <- tiempos, d == (dorsal co)] --foldl sumaTiempos (obtTiempos cs co) [x | (d, x) <- tiempos, d == (dorsal co)]

-- Suma dos tiempos atendiendo a las características propias del sistema de horas, minutos y segundos
sumaTiempos :: Tiempo -> Tiempo -> Tiempo
sumaTiempos (h1, m1, s1) (h2, m2, s2) = (hora, minuto, segundo) where
                    segundo = mod s3 60
                    s3 = s1 + s2
                    minuto = mod m3 60
                    m3 = m1 + m2 + (div s3 60)
                    hora = h1 + h2 + (div m3 60)

ordenaClasificacion :: [Clasificacion] -> [Clasificacion]
ordenaClasificacion [] = []
ordenaClasificacion ((e, n, t):lc) = ordenaClasificacion [(eq, nom, tp) | (eq, nom, tp) <- lc, tp < t] ++ [(e, n, t)] ++ ordenaClasificacion [(eq, nom, tp) | (eq, nom, tp) <- lc, tp >= t]

ordenaClasificacionEquipos :: [(Equipo, Tiempo)] -> [(Equipo, Tiempo)]
ordenaClasificacionEquipos [] = []
ordenaClasificacionEquipos ((e, t):lc) = ordenaClasificacionEquipos [(eq, tp) | (eq, tp) <- lc, tp < t] ++ [(e, t)] ++ ordenaClasificacionEquipos [(eq, tp) | (eq, tp) <- lc, tp >= t]

mostrarParticipantes :: Participantes -> IO ()
mostrarParticipantes p = do
                            putStr "EQUIPO                   CORREDOR                 DORSAL\n"
                            putStr (_mostrarParticipantes p)

_mostrarParticipantes :: Participantes -> String
_mostrarParticipantes [] = ""
_mostrarParticipantes (p:ps) = (show (equipo p)) ++ (blancos (25 - length (show (equipo p)))) ++ (show (nom p)) ++ (blancos (25 - length (show (nom p)))) ++ (show (dorsal p)) ++ "\n" ++ (_mostrarParticipantes ps)

blancos :: Int -> String
blancos 0 = ""
blancos n = " " ++ blancos (n-1)

mostrarCarrera :: Carrera -> IO()
mostrarCarrera [] = putStr ""
mostrarCarrera (c:cs) = do
                          putStr ("ETAPA: " ++ (show (num c)) ++ (blancos (4 - length (show (num c)))))
                          putStr ("TIPO: " ++ (show (tipo c)))
                          putStr "\n"
                          putStr "DORSAL     TIEMPO\n"
                          putStr (mostrarTiempos (tiempos c))
                          putStr "\n"
                          mostrarCarrera cs

mostrarTiempos :: TiemposEtapa -> String
mostrarTiempos [] = ""
mostrarTiempos ((d, (h,m,s)):xs) = show d ++ (blancos (11 - (length (show d)))) ++ (show h) ++ ":" ++ (show m) ++ ":" ++ (show s) ++ "\n" ++ mostrarTiempos xs

mostrarClasifCorr :: Carrera -> Participantes -> IO()
mostrarClasifCorr c p = do
                        putStr "CLASIFICACION POR CORREDOR\n"
                        putStr "EQUIPO                    NOMBRE                    TIEMPO\n"
                        putStr (_mostrarClasifCorr (obtClasificacion c p))

_mostrarClasifCorr :: [Clasificacion] -> String
_mostrarClasifCorr [] = ""
_mostrarClasifCorr ((e,n,(h,m,s)):xs) = show e ++ (blancos (26 - (length (show e)))) ++ show n ++ (blancos (26 - (length (show n)))) ++ (show h) ++ ":" ++ (show m) ++ ":" ++ (show s) ++ "\n" ++ _mostrarClasifCorr xs

mostrarClasifEqui :: Carrera -> Participantes -> IO()
mostrarClasifEqui c p = do
                        putStr "CLASIFICACION POR CORREDOR\n"
                        putStr "EQUIPO                    TIEMPO\n"
                        putStr (_mostrarClasifEqui (obtClasifEquipos c p))

_mostrarClasifEqui :: [(Equipo, Tiempo)] -> String
_mostrarClasifEqui [] = ""
_mostrarClasifEqui ((e,(h,m,s)):xs) = show e ++ (blancos (26 - (length (show e)))) ++ (show h) ++ ":" ++ (show m) ++ ":" ++ (show s) ++ "\n" ++ _mostrarClasifEqui xs

mostrarLiderTipo :: Carrera -> Participantes -> TipoEtapa -> IO()
mostrarLiderTipo c p t = if nombre /= "" then
                          do
                             putStr "LIDER DE LAS ETAPAS DE TIPO "
                             putStr (show t ++ "\n")
                             putStr "NOMBRE                    EQUIPO\n"
                             putStr (nombre ++ (blancos (26 - (length (nombre)))) ++ (equipo))
                          else putStr "No hay ninguna etapa de ese tipo"
                          where (nombre, equipo) = obtLiderTipo c p t

mostrarLider :: Carrera -> Participantes -> IO()
mostrarLider c p = if nombre /= "" then
                    do
                      putStr "LIDER DE LA CARRERA\n"
                      putStr "NOMBRE                    EQUIPO\n"
                      putStr (nombre ++ (blancos (26 - (length (nombre)))) ++ (equipo))
                   else putStr "Aun no se ha corrido ninguna etapa"
                   where (nombre, equipo) = obtLider c p

mostrarAbandonos :: Carrera -> Participantes -> IO()
mostrarAbandonos c p = if cadena /= "" then
                        do
                          putStr "NOMBRE                    ETAPA\n"
                          putStr cadena
                        else putStr "No hay abandonos"
                        where cadena = _mostrarAbandonos (retiradosCarrera p c)

_mostrarAbandonos :: [(Nombre, Int)] -> String
_mostrarAbandonos [] = ""
_mostrarAbandonos ((n, e):xs) = n ++ (blancos (26 - (length (n)))) ++ (show e) ++ "\n" ++ _mostrarAbandonos xs
